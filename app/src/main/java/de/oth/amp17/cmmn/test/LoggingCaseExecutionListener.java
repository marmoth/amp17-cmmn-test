package de.oth.amp17.cmmn.test;

import org.camunda.bpm.engine.delegate.CaseExecutionListener;
import org.camunda.bpm.engine.delegate.DelegateCaseExecution;

public class LoggingCaseExecutionListener implements CaseExecutionListener {

    @Override
    public void notify(DelegateCaseExecution caseExecution) throws Exception {
        String eventName = caseExecution.getEventName();
        String caseDefinitionId = caseExecution.getCaseDefinitionId();
        String activityId = caseExecution.getActivityId();
        System.out.printf("Case: %s, Activity: %s, Event: %s%n", caseDefinitionId, activityId, eventName);
    }
}
