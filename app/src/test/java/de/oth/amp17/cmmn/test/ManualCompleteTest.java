/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oth.amp17.cmmn.test;

import org.camunda.bpm.engine.impl.test.CmmnProcessEngineTestCase;
import org.camunda.bpm.engine.runtime.CaseExecutionQuery;
import org.camunda.bpm.engine.runtime.CaseInstance;
import org.camunda.bpm.engine.test.Deployment;

/**
 * @author Roman Smirnov
 */
public class ManualCompleteTest extends CmmnProcessEngineTestCase {

    protected static final String CASE_DEFINITION_KEY = "case";

    @Deployment
    public void testCaseModel1() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        disable(queryCaseExecutionByActivityId("taskB").getId());

        caseService.completeCaseExecution(caseInstanceId);
    }

    @Deployment(resources = "de/oth/amp17/cmmn/test/ManualCompleteTest.testCaseModel1.cmmn")
    public void testCaseModel1_2() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        manualStart(queryCaseExecutionByActivityId("taskA").getId());
        disable(queryCaseExecutionByActivityId("taskB").getId());

        caseService.completeCaseExecution(caseInstanceId);

    }

    @Deployment(resources = "de/oth/amp17/cmmn/test/ManualCompleteTest.testCaseModel1.cmmn")
    public void testCaseModel1_3() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        manualStart(queryCaseExecutionByActivityId("taskA").getId());

        caseService.terminateCaseExecution(caseInstanceId);

    }
}
