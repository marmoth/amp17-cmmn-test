/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package de.oth.amp17.cmmn.test;

import org.camunda.bpm.engine.impl.test.CmmnProcessEngineTestCase;
import org.camunda.bpm.engine.runtime.CaseExecution;
import org.camunda.bpm.engine.runtime.CaseExecutionQuery;
import org.camunda.bpm.engine.runtime.CaseInstance;
import org.camunda.bpm.engine.runtime.CaseInstanceQuery;
import org.camunda.bpm.engine.test.Deployment;
import org.junit.Test;

/**
 * @author Roman Smirnov
 */
public class AutoCompleteTest extends CmmnProcessEngineTestCase {

    protected static final String CASE_DEFINITION_KEY = "case";

    @Deployment
    public void testCasePlanModel() {
        // given
        // a deployed process

        // when
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        // then
        CaseInstance caseInstance = caseService
                .createCaseInstanceQuery()
                .caseInstanceId(caseInstanceId)
                .singleResult();

        assertNotNull(caseInstance);
        assertTrue(caseInstance.isCompleted());

        // humanTask1 and humanTask2 are not available
        CaseExecutionQuery query = caseService.createCaseExecutionQuery();
        assertNull(query.activityId("PI_HumanTask_1").singleResult());
        assertNull(query.activityId("PI_HumanTask_2").singleResult());
    }

    @Deployment
    public void testStage() {
        // given
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        CaseExecutionQuery query = caseService
                .createCaseExecutionQuery()
                .activityId("PI_Stage_1");

        String stageId = query.singleResult().getId();

        // when
        caseService.manuallyStartCaseExecution(stageId);

        // then

        // the instance is still active (contains
        // a further human task)
        CaseInstance caseInstance = caseService
                .createCaseInstanceQuery()
                .caseInstanceId(caseInstanceId)
                .singleResult();
        assertNotNull(caseInstance);
        assertTrue(caseInstance.isActive());

        // humanTask1 is still available
        assertNotNull(query.activityId("PI_HumanTask_1").singleResult());

        // stage, humanTask2, humanTask3 are not available
        assertNull(query.activityId("PI_Stage_1").singleResult());
        assertNull(query.activityId("PI_HumanTask_2").singleResult());
        assertNull(query.activityId("PI_HumanTask_3").singleResult());
    }

    @Deployment
    public void testNotAutoComplete1() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
        complete(queryCaseExecutionByActivityId("taskB").getId());
    }

    @Deployment
    public void testNotAutoComplete2() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
        disable(queryCaseExecutionByActivityId("taskB").getId());
    }

    @Deployment
    public void testNotAutoComplete3() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
//        disable(queryCaseExecutionByActivityId("taskB").getId());
    }

    @Deployment
    public void testAutoComplete1() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
        disable(queryCaseExecutionByActivityId("taskB").getId());
    }

    @Deployment
    public void testAutoComplete2() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
        disable(queryCaseExecutionByActivityId("taskB").getId());
    }

    @Deployment(resources = "de/oth/amp17/cmmn/test/AutoCompleteTest.testAutoComplete2.cmmn")
    public void testAutoComplete2_2() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        complete(queryCaseExecutionByActivityId("taskA").getId());
        String taskB = queryCaseExecutionByActivityId("taskB").getId();
        manualStart(taskB);
        terminate(taskB);
    }

    @Deployment
    public void testNotAutoCompleteExit1() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        String taskA = queryCaseExecutionByActivityId("taskA").getId();
        manualStart(taskA);
        complete(taskA);
    }

    @Deployment
    public void testNotAutoCompleteExit2() {
        String caseInstanceId = createCaseInstanceByKey(CASE_DEFINITION_KEY).getId();

        String taskA = queryCaseExecutionByActivityId("taskA").getId();
        disable(taskA);
    }
}
